//import Jama.*; //found at http://math.nist.gov/javanumerics/jama/Jama-1.0.3.jar

public class Main
{
	private static MyMatrix matrixA  = new MyMatrix(new double[][] { { 0.9, 0.1 },
																 	 { 0.2, 0.8 }, });

	private static MyMatrix matrixV0 = new MyMatrix(new double[][] { { 0.6, 0.4 } });

	public static void main (String[] args)
	{
		for ( int i = 1; i < 20; i++)
		{
			MyMatrix tmp = matrixV0;
			MyMatrix result = tmp.multiply (matrixA.pow (i)); //Vn = V0 * A^n
			System.out.print (i + ": ");
			result.print(5, 4);		
		}
	}

	@SuppressWarnings("unused")
	private static void printSeperator ()
	{
		System.out.println("-------------");
	}
}
