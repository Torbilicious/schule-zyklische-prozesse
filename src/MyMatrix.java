import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

public class MyMatrix
{
	private double[][] A;

	private int m, n;

	public MyMatrix ()
	{

	}

	public MyMatrix (int m, int n)
	{
		this.m = m;
		this.n = n;
		A = new double[m][n];
	}

	public MyMatrix (double[][] data)
	{
		m = data.length;
		n = data[0].length;

		this.A = data;
	}

	public MyMatrix multiply (MyMatrix B)
	{
		MyMatrix X = new MyMatrix (m, B.n);
		double[][] C = X.getArray ();
		double[] Bcolj = new double[n];

		for (int j = 0; j < B.n; j++)
		{
			for (int k = 0; k < n; k++)
			{
				Bcolj[k] = B.A[k][j];
			}

			for (int i = 0; i < m; i++)
			{
				double[] Arowi = A[i];
				double s = 0;

				for (int k = 0; k < n; k++)
				{
					s += Arowi[k] * Bcolj[k];
				}

				C[i][j] = s;
			}
		}

		return X;
	}

	public MyMatrix pow (int times)
	{
		MyMatrix result = this;

		for (int i = 1; i < times; i++)
			result = result.multiply (this);

		return result;
	}

	public double[][] getArray ()
	{
		return A;
	}

	public void print (int w, int d)
	{
		print (new PrintWriter (System.out, true), w, d);
	}

	public void print (PrintWriter output, int w, int d)
	{
		DecimalFormat format = new DecimalFormat ();
		format.setDecimalFormatSymbols (new DecimalFormatSymbols (Locale.GERMAN));
		format.setMinimumIntegerDigits (1);
		format.setMaximumFractionDigits (d);
		format.setMinimumFractionDigits (d);
		format.setGroupingUsed (false);
		print (output, format, w + 2);
	}

	public void print (NumberFormat format, int width)
	{
		print (new PrintWriter (System.out, true), format, width);
	}

	public void print (PrintWriter output, NumberFormat format, int width)
	{
		output.println (); // start on new line.
		
		for (int i = 0; i < m; i++)
		{
			for (int j = 0; j < n; j++)
			{
				String s = format.format (A[i][j]); // format the number
				int padding = Math.max (1, width - s.length ()); // At _least_ 1
																	// space
				for (int k = 0; k < padding; k++)
					output.print (' ');
				output.print (s);
			}
			
			output.println ();
		}
		
		output.println (); // end with blank line.
	}
}
